'use strict';

/* Controllers */

angular.module('gnwebsite.controllers', []).
  controller('AppCtrl', function ($scope, $http) {

    $http({
      method: 'GET',
      url: '/api/name'
    }).
    success(function (data, status, headers, config) {
      $scope.name = data.name;
    }).
    error(function (data, status, headers, config) {
      $scope.name = 'Error!'
    });
  })
.controller('MovieCtrl', function ($scope) {

  $scope.numSelected = 0

  $scope.firstIndex = 0;
  $scope.lastIndex = 0;

  $scope.invertSelection = function () {

      $('div[data-reviewstate]').find('.movieName').each(function() {
          $scope.selectReview($(this).data("reviewid"));
      });
  };

  $scope.saveSelection = function () {
    // $('.saveHeader:first-child').each(function () {
    //     var elem = $(this);
    //     elem.removeClass('saveHeader');
    //     elem.find('.selectButton').removeClass('saveHeader');
    //     elem.data('reviewstate', 0);
    // });
    $('.selectHeader').not('.selectButton').each(function (index,element) {
        var elem = $(element);
        elem.removeClass('selectHeader');
        elem.find('.selectButton').removeClass('selectHeader');
        elem.addClass('saveHeader');
        elem.find('.selectButton').addClass('saveHeader');
        elem.find('i').removeClass('icon-star-empty');
        elem.find('i').addClass('icon-remove');
        elem.data('reviewstate', 2);
        $scope.numSelected--;
    });

  };

  $scope.getReviewContent = function (bugID, event) {

    var dataBugID = '"' + bugID + '"';

    var content = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper porta. Mauris massa. Vestibulum lacinia arcu eget nulla. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Curabitur sodales ligula in libero. Sed dignissim lacinia nunc. Curabitur tortor. Pellentesque nibh. Aenean quam. In scelerisque sem at dolor. Maecenas mattis. Sed convallis tristique sem. Proin ut ligula vel nunc egestas porttitor. Morbi lectus risus, iaculis vel, suscipit quis, luctu."

    var titleElem = $('div[data-reviewid=' + dataBugID + ']');
    
    if(titleElem.data("gotreview") != true)
    {
      titleElem.parent().next().append('<div class="movieDescription" hidden=true data-descreviewid=' + dataBugID +'>' + content + "</p>");
    }

    titleElem.data("gotreview",true);
    var descriptionElem = $('div[data-descreviewid=' + dataBugID + ']');
    descriptionElem.slideToggle();
  };

  $scope.pressSelect = function (bugID)
  {
    var dataBugID = '"' + bugID + '"';
    var selectElem = $('div[data-reviewid=' + dataBugID + ']').next();
    var parentElem = selectElem.parent();

    if(parentElem.data("reviewstate") == 2)
    {
      selectElem.removeClass("saveHeader");
      parentElem.removeClass("saveHeader");
      selectElem.find("i").removeClass("icon-remove");
      selectElem.find("i").addClass("icon-star-empty");
      parentElem.data("reviewstate", 0);
    }
    else
    {
      $scope.selectReview(bugID);
    }
  }

  $scope.selectReview = function (bugID) {
    var dataBugID = '"' + bugID + '"';
    var titleElem = $('div[data-reviewid=' + dataBugID + ']');
    var selectElem = titleElem.next();
    var parentElem = titleElem.parent();

    if(parentElem.data("reviewstate") != 2)
    {
      selectElem.toggleClass("selectHeader");
      parentElem.toggleClass("selectHeader");
    }

    if(parentElem.data("reviewstate") == 0)
    {
      $scope.numSelected++;
      parentElem.data("reviewstate", 1);
    }
    else if(parentElem.data("reviewstate") == 1)
    {
      parentElem.data("reviewstate", 0);
      $scope.numSelected--;
    }
  }

  $scope.reviews = [{title:"High Fidelity",reviewid:1},
                      {title:"Pitch Perfect",reviewid:2},
                      {title:"Eternal Sunshine of the Spotless Mind",reviewid:3},
                      {title:"Good Will Hunting",reviewid:4},
                      {title:"The Hobbit",reviewid:5},
                      {title:"21 Jump Street",reviewid:6},
                      {title:"50/50",reviewid:7},
                      {title:"Superbad",reviewid:8},
                      {title:"Zombieland",reviewid:9},
                      {title:"The Other Guys",reviewid:10},
                      {title:"Monty Python and the Holy Grail",reviewid:11},
                      {title:"Bruce Almighty",reviewid:12},
                      {title:"Hitchhiker's Guide to the Galaxy",reviewid:13},
                      {title:"Office Space",reviewid:14},
                      {title:"Ferris Bueller's Day Off",reviewid:15},
                      {title:"When Harry Met Sally",reviewid:16},
                      {title:"Annie Hall",reviewid:17},
                      {title:"Mall Cop",reviewid:18},
                      {title:"Starsky and Hutch",reviewid:19},
                      {title:"My Best Friend's Girl",reviewid:20},
                      {title:"Meet the Fockers",reviewid:21},
                      {title:"Ghostbusters",reviewid:22},
                      {title:"Be Kind Rewind",reviewid:23},
                      {title:"Scary Movie 4",reviewid:24}];
})
.controller('MovieHeaderCtrl', function ($scope) {

})
.controller('HomeCtrl', function ($scope) {

})
.controller('HomeHeaderCtrl', function ($scope) {

})
.controller('BlankCtrl', function ($scope) {
  var paper = Raphael("#drawingBoard", 200,200);
  $('#drawingBoard').on('mousedown', function(event) {
    paper.circle(event.clientX, event.clientY,2);
  });
})
.controller('BlogCtrl', function ($scope) {
  
});


