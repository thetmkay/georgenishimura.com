'use strict';

// Declare app level module which depends on filters, and services

angular.module('gnwebsite', [
  'gnwebsite.controllers',
  'gnwebsite.filters',
  'gnwebsite.services',
  'gnwebsite.directives',
  'ui.router'
]).
config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function ($stateProvider, $urlRouterProvider, $locationProvider) {
  $urlRouterProvider.otherwise('/maintenance');

  $stateProvider
  .state('movies', {
    url:'/movies',
    views: {
      'mainView' : {
        templateUrl: 'partials/movies',
        controller: 'MovieCtrl'
      },
      'mainHeader' : {
        templateUrl: 'partials/movieheader',
        controller: 'MovieCtrl'
      },
    }
  }).
  state('home', {
    url:'/home',
    views: {
      'mainView' : {
        templateUrl: 'partials/home',
        controller: 'HomeCtrl'
      },
      'mainHeader' : {
        templateUrl: 'partials/homeheader',
        controller: 'HomeHeaderCtrl'
      },
    }
  }).
  state('maintenance', {
    url:'/maintenance',
    views: {
      'mainView' : {
        templateUrl: 'partials/maintenance',
        controller: 'MaintenanceCtrl'
      },
    }
  }).
  state('blog', {
    url:'/blog',
    views: {
      'mainView' : {
        templateUrl: 'partials/blog',
        controller: 'BlogCtrl'
      },
      'mainHeader' : {
        tempateUrl: 'partials/blogheader',
        controller: 'BlogCtrl'
      } 
    }
  });

  
  

  $locationProvider.html5Mode(true);
}]);
